import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * This class provides a shortestPath method for finding routes between two points
 * on the map. Start by using Dijkstra's, and if your code isn't fast enough for your
 * satisfaction (or the autograder), upgrade your implementation by switching it to A*.
 * Your code will probably not be fast enough to pass the autograder unless you use A*.
 * The difference between A* and Dijkstra's is only a couple of lines of code, and boils
 * down to the priority you use to order your vertices.
 */
public class Router {
    /**
     * Return a LinkedList of <code>Long</code>s representing the shortest path from st to dest,
     * where the longs are node IDs.
     */
    private static PriorityQueue<Node> priorityDist;

    public static LinkedList<Long> shortestPath(GraphDB g, double stlon, double stlat,
                                                double destlon, double destlat) {

        Comparator<Node> nodeOrder = new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                double disto1 = o1.priority;
                double disto2 = o2.priority;
                if (disto2 > disto1) {
                    return -1;
                } else if (disto2 < disto1) {
                    return 1;
                } else {
                    return 0;
                }
            }
        };

        priorityDist = new PriorityQueue<Node>(nodeOrder);
        long startNodeID = g.closest(stlon, stlat);
        long destNodeID = g.closest(destlon, destlat);
        int startVisit = g.getVisit();

        Node startNode = g.getNode(startNodeID);
        startNode.distance = 0.0;
        priorityDist.add(startNode);

        Node destNode = g.getNode(destNodeID);

        startNode.clean();
        destNode.clean();

        double currDist;
        long currID;
        double currCost;

        while (!priorityDist.isEmpty()) {

            Node currNode = priorityDist.peek();
            currID = currNode.id;
            currNode.visited = startVisit;
            currDist = currNode.distance;
            currCost = currNode.priority;
            priorityDist.remove(currNode);

            if (currCost >= destNode.distance) {
                break;
            }

            for (long adjNodeID : g.adjacent(currID)) {
                Node adjNode = g.getNode(adjNodeID);
                double totalDist = currDist + g.distance(currID, adjNodeID);
                if (adjNode.visited != startVisit && adjNode.distance > totalDist) {
                    adjNode.priority = calcPriority(g, adjNodeID, destNodeID, totalDist);
                    adjNode.distance = totalDist;
                    adjNode.prev = currID;
                    priorityDist.add(adjNode);
                }
            }

        }

        return shortestPathIDs(g, destNodeID, startNodeID);
    }

    private static LinkedList<Long> shortestPathIDs(GraphDB g, long destNodeID, long startNodeID) {
        LinkedList<Long> temp = new LinkedList<Long>();
        long currNodeID = destNodeID;
        while (currNodeID != startNodeID) {
            temp.addFirst(currNodeID);
            if (currNodeID == 0) {
                break;
            }
            currNodeID = g.getNode(currNodeID).prev;
        }
        temp.addFirst(startNodeID);
        return temp;
    }

    private static double calcPriority(GraphDB g, long adj, long end, double prevDist) {
        return prevDist + g.distance(adj, end);
    }

    /*public static void main(String[] args) {
        GraphDB temp = new GraphDB("berkeley.osm");
        //{end_lat=37.83796678748061,
        //start_lon=-122.26143030962002, start_lat=37.831051606162895,
        //end_lon=-122.2650498760774}.end_lat=37.83796678748061,
        System.out.println(shortestPath(temp,
                -122.26143030962002, 37.831051606162895,
                -122.2650498760774, 37.83796678748061));


    }*/
}
