import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.PriorityQueue;

public class QTree {
    Node root; //root of the QTree
    ArrayList<Node> temp;
    String[][] qFString;
    double rasterULLon, rasterULLat, rasterLRLon, rasterLRLat;

    QTree(double ulLon, double ulLat, double lrLon, double lrLat,
               int depth, String fileName, double width) {
        this.root = new Node(ulLon, ulLat, lrLon, lrLat,
                    depth, fileName, width);
    }

    public String[][] queryField(double query_ulLon, double query_ulLat,
                                 double query_lrLon, double query_lrLat,
                                 double query_width){
        double query_lonDPP = (query_lrLon - query_ulLon)/query_width;
        this.temp = new ArrayList<Node>();
        this.root.intersect(query_ulLon, query_ulLat, query_lrLon, query_lrLat, query_lonDPP, this.root.depth);
        this.qFString = convArList(this.temp);
        return qFString;
    }

    private String[][] convArList(ArrayList<Node> temp){
        int width = 0;
        int height = 0;
        PriorityQueue allLat = new PriorityQueue(Collections.reverseOrder());
        double baseULLat = temp.get(0).ulLat;
        double baseULLon = temp.get(0).ulLon;
        allLat.add(baseULLat);
        for (Node child : temp){
            if (baseULLat == child.ulLat){
                width++;
            }
            if (baseULLon == child.ulLon) {
                height++;
                if (!allLat.contains(child.ulLat)){
                    allLat.add(child.ulLat);
                }
            }
        }
        String[][] map = new String[height][width];

        for (int i = 0; i < height; i++){
            String[] row = new String[width];
            int rowCount = 0;
            double baseLat = (double) allLat.peek();
            for (Node child : temp){
                if (baseLat == child.ulLat){
                    if (rowCount == 0 && i == 0) {
                        rasterULLon = child.ulLon;
                        rasterULLat = child.ulLat;
                    }
                    if (rowCount == width - 1 && i == height - 1) {
                        rasterLRLon = child.lrLon;
                        rasterLRLat = child.lrLat;
                    }
                    row[rowCount] = "img/"+child.fileName+".png";
                    rowCount++;
                    if (rowCount == width){
                        allLat.remove(baseLat);
                        map[i] = row;
                        break;
                    }
                }
            }

        }
        //System.out.println("Testing: "+map[0][0]);

        return map;
    }

    public int getRasterDepth() {
        return this.temp.get(0).depth;
    }

    public double getRasterULLon() {
        return this.rasterULLon;
    }

    public double getRasterULLat() {
        return this.rasterULLat;
        //return this.temp.get(0).ulLat;
    }

    public double getRasterLRLon() {
        return this.rasterLRLon;
        //return this.temp.get(this.temp.size()-1).lrLon;
    }

    public double getRasterLRLat() {
        return this.rasterLRLat;
        //return this.temp.get(this.temp.size()-1).lrLat;
    }

    private class Node{
        int depth;
        String fileName;
        double lonDPP; //lonDPP of the layer;
        double lonMid, latMid;
        double ulLon, ulLat, lrLon, lrLat; //coordinates of the image
        Node uLeft, uRight, lLeft, lRight; //children of the node
        ArrayList<Node> children; //for iteration purposes

        Node(double ulLon, double ulLat, double lrLon, double lrLat,
             int depth, String fileName, double width){
            //this.depth = depth;
                this.ulLon = ulLon;
                this.ulLat = ulLat;
                this.lrLon = lrLon;
                this.lrLat = lrLat;
                this.depth = depth;
                //this.depth = depth; initial depth is 1
                this.fileName = fileName;
                this.lonDPP = (lrLon - ulLon) / (width);

                this.lonMid = ulLon + (lrLon - ulLon) / 2;
                this.latMid = lrLat + (ulLat - lrLat) / 2;

                //System.out.println("The lonDPP of layer "+this.depth+ " is: "+this.lonDPP);
            if (depth <= 7) {
                this.uLeft = new Node(ulLon, ulLat, lonMid, latMid,
                        this.depth+1, fileName+"1", width);
                this.uRight = new Node(lonMid, ulLat, lrLon, latMid,
                        this.depth+1, fileName+"2", width);
                this.lLeft = new Node(ulLon, latMid, lonMid, lrLat,
                        this.depth+1, fileName+"3", width);
                this.lRight = new Node(lonMid, latMid, lrLon, lrLat,
                        this.depth+1, fileName+"4", width);

                this.children = new ArrayList<Node>();
                this.children.add(uLeft);
                this.children.add(uRight);
                this.children.add(lLeft);
                this.children.add(lRight);
            }
        }

        public void intersect(double query_ulLon, double query_ulLat,
                              double query_lrLon, double query_lrLat,
                              double query_lonDPP, int depth){
            if (this.depth == 7 || (this.lonDPP <= query_lonDPP)) {
                temp.add(this);
            } else {
                for (Node child : this.children){
                    if (isOverlap(child, query_ulLon, query_ulLat, query_lrLon, query_lrLat)) {
                        child.intersect(query_ulLon, query_ulLat, query_lrLon, query_lrLat,
                                query_lonDPP, this.depth);
                    }
                }
            }
        }


        private boolean isOverlap(Node node, double query_ulLon, double query_ulLat,
                                  double query_lrLon, double query_lrLat){
            double llx = query_ulLon;   // lower left x
            double lly = query_lrLat;   // lower left y
            double urx = query_lrLon;   // upper right x
            double ury = query_ulLat;   // upper right y

            double llx2 = node.ulLon;   // lower left x
            double lly2 = node.lrLat;   // lower left y
            double urx2 = node.lrLon;   // upper right x
            double ury2 = node.ulLat;   // upper right y

            if (llx <= urx2 && urx >= llx2 && lly <= ury2 && ury >= lly2) {
                return true;
            } else {
                return false;
            }
        }
    }

    /*For QTree testing purposes*/
    /*public static void main(String[] args) {
        QTree temp = new QTree(MapServer.ROOT_ULLON, MapServer.ROOT_ULLAT,
                MapServer.ROOT_LRLON, MapServer.ROOT_LRLAT, 0, "",
                MapServer.TILE_SIZE);

        //double qdpp = (-122.22275132672245+122.23995662778569)/613.0;
        //System.out.println("querylonDPP: "+qdpp);

        temp.queryField(-122.23995662778569, 37.877266154010954,
                -122.22275132672245, 37.85829260830337, 613.0);

        System.out.println("Size of the arraylist node: "+temp.temp.size());
        System.out.println("Depth: "+temp.getRasterDepth());
        System.out.println("lonDPP: "+temp.temp.get(0).lonDPP);

        for (int i = 0; i<temp.qFString.length; i++){
            String[] row = temp.qFString[i];
            System.out.print("Row "+i+": ");
            for (int j = 0; j<row.length; j++){
                System.out.print(row[j]+", ");
            }
            System.out.println("");
        }}*/

        //Expected: raster_ul_lon=-122.2998046875,
            //raster_ul_lat=37.892195547244356,
            // raster_lr_lon=-122.2119140625,
            // raster_lr_lat=37.82280243352756,

            //depth = 4;

        //Got: raster_ul_lon=-122.244873046875,
        // raster_ul_lat=37.87918433842246
        // raster_lr_lon=-122.2174072265625,
        // raster_lr_lat=37.85749899038596,
        // depth=4,
    //}

        /*1234test
        temp.queryField(-122.3027284165759, 37.88708748276975,
                -122.20908713544797, 37.848731523430196, 305.0);

        System.out.println("Size of the arraylist node: "+temp.temp.size());

        String[] row1 = temp.qFString[0];
        String[] row2 = temp.qFString[1];

        System.out.println("Row 1: "+row1[0]+", "+row1[1]);
        System.out.println("Row 2: "+row2[0]+", "+row2[1]);

        System.out.println("Depth is: "+temp.getRasterDepth());
*/

        /*test
        temp.queryField(-122.24163047377972, 37.87655856892288,
                -122.24053369025242, 37.87548268822065, 892.0);

        System.out.println("Size of the arraylist node: "+temp.temp.size());

        String[] row1 = temp.qFString[0];
        String[] row2 = temp.qFString[1];
        String[] row3 = temp.qFString[2];

        System.out.println("Row 1: "+row1[0]+", "+row1[1]+", "+row1[2]);
        System.out.println("Row 2: "+row2[0]+", "+row2[1]+", "+row2[2]);
        System.out.println("Row 3: "+row3[0]+", "+row3[1]+", "+row3[2]);

        System.out.println("Depth is: "+temp.getRasterDepth());*/

        /*twelvetest
        temp.queryField(-122.30410170759153, 37.870213571328854,
                -122.2104604264636, 37.8318576119893, 1091.0);

        String[] row1 = temp.qFString[0];
        String[] row2 = temp.qFString[1];
        String[] row3 = temp.qFString[2];

        System.out.println("Row 1: "+row1[0]+", "+row1[1]+", "+row1[2]+", "+row1[3]);
        System.out.println("Row 2: "+row2[0]+", "+row2[1]+", "+row2[2]+", "+row2[3]);
        System.out.println("Row 3: "+row3[0]+", "+row3[1]+", "+row3[2]+", "+row3[3]);

        System.out.println("Depth is: "+temp.getRasterDepth());*/


}
