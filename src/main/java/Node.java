import java.util.ArrayList;

/*
 Node for GraphDB
 */
public class Node {
    double longitude;
    double latitude;
    double distance;
    long id, prev;
    int visited;
    double priority; //cost
    String locName;
    ArrayList<Long> adjacent; //contains all adjacent nodeIDs to the current node

    Node(double longitude, double latitude, long id){
        this.longitude = longitude;
        this.latitude = latitude;
        this.id = id;
        //this.prev = id;
        this.prev = 0;
        this.visited = 0;
        this.distance = Double.POSITIVE_INFINITY;
        //this.priority = Double.POSITIVE_INFINITY;
        //this.connected = false;
        this.adjacent = new ArrayList<Long>();
        this.locName = "";
    }

    void addName(String name){
        this.locName = name;
    }

    void clean() {
        this.prev = 0;
    }
}
