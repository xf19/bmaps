import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Graph for storing all of the intersection (vertex) and road (edge) information.
 * Uses your GraphBuildingHandler to convert the XML files into a graph. Your
 * code must include the vertices, adjacent, distance, closest, lat, and lon
 * methods. You'll also need to include instance variables and methods for
 * modifying the graph (e.g. addNode and addEdge).
 *
 * @author Alan Yao, Josh Hug
 */
public class GraphDB {
    /**
     * Your instance variables for storing the graph. You should consider
     * creating helper classes, e.g. Node, Edge, etc.
     */
    private HashMap<Long, Node> allNodes;
    private int visitNum;

    /**
     * Example constructor shows how to create and start an XML parser.
     * You do not need to modify this constructor, but you're welcome to do so.
     *
     * @param dbPath Path to the XML file to be parsed.
     */
    public GraphDB(String dbPath) {
        try {
            this.allNodes = new HashMap<Long, Node>();
            this.visitNum = 0;
            File inputFile = new File(dbPath);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            GraphBuildingHandler gbh = new GraphBuildingHandler(this);
            saxParser.parse(inputFile, gbh);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        clean();
    }

    /**
     * Helper to process strings into their "cleaned" form, ignoring punctuation and capitalization.
     *
     * @param s Input string.
     * @return Cleaned string.
     */
    static String cleanString(String s) {
        return s.replaceAll("[^a-zA-Z ]", "").toLowerCase();
    }

    public void addNode(String lon, String lat, String id) {
        double newLon = Double.parseDouble(lon);
        double newLat = Double.parseDouble(lat);
        long newId = Long.parseLong(id);
        Node temp = new Node(newLon, newLat, newId);
        this.allNodes.put(newId, temp);
    }

    public void connectWayPairs(ArrayList<GraphBuildingHandler.wayPairs> wayPairsList) {
        for (GraphBuildingHandler.wayPairs pair : wayPairsList) {
            connectWay(pair.prev, pair.curr);
        }
    }

    public void connectWay(long prevID, long currID) {
        if (prevID == 0) {
            return;
        }

        Node prev = getNode(prevID);
        Node curr = getNode(currID);

        prev.adjacent.add(currID);
        curr.adjacent.add(prevID);
    }

    public void addLocName(long nodeID, String name) {
        getNode(nodeID).addName(name);
    }

    private void remove(long nodeID) {
        this.allNodes.remove(nodeID);
    }

    /**
     * Remove nodes with no connections from the graph.
     * While this does not guarantee that any two nodes in the remaining graph are connected,
     * we can reasonably assume this since typically roads are connected.
     */
    private void clean() {
        Iterable<Long> allNodesIter = this.vertices();
        for (Long nodeID : allNodesIter) {
            Node temp = this.allNodes.get(nodeID);
            if (temp.adjacent.isEmpty()) {
                //System.out.println("The size of "+nodeID+"'s adjacents: "+temp.adjacent.size());
                this.remove(nodeID);
            }
        }
    }

    /**
     * Returns an iterable of all vertex IDs in the graph.
     */
    Iterable<Long> vertices() {
        return new ArrayList<Long>(this.allNodes.keySet());
    }

    /**
     * Returns ids of all vertices adjacent to v.
     */
    Iterable<Long> adjacent(long v) {
        return getNode(v).adjacent;
    }

    /**
     * Returns the Euclidean distance between vertices v and w, where Euclidean distance
     * is defined as sqrt( (lonV - lonV)^2 + (latV - latV)^2 ).
     */
    double distance(long v, long w) {
        double xPow = Math.pow(((this.lon(w) - this.lon(v))), 2);
        double yPow = Math.pow(((this.lat(w) - this.lat(v))), 2);
        return Math.sqrt(xPow + yPow);
    }

    /**
     * Returns the vertex id closest to the given longitude and latitude.
     */
    long closest(double lon, double lat) {
        long shortID = 0;
        double closest = Double.POSITIVE_INFINITY;
        long tempID = 0;
        Node tempNode = new Node(lon, lat, tempID);
        this.allNodes.put(tempID, tempNode);

        Iterable<Long> allNodesIt = this.vertices();
        for (Long nodeID : allNodesIt) {
            if (!(nodeID == tempID)) {
                double temp = distance(tempID, nodeID);
                if (closest > temp) {
                    shortID = nodeID;
                    closest = temp;
                }
            }
        }
        this.remove(tempID);
        return shortID;
    }

    /**
     * Longitude of vertex v.
     */
    double lon(long v) {
        return getNode(v).longitude;
    }

    /**
     * Latitude of vertex v.
     */
    double lat(long v) {
        return getNode(v).latitude;
    }

    /*Return a node v*/
    Node getNode(long v) {
        return this.allNodes.get(v);
    }

    /*Return visitNum for startVisit*/
    int getVisit() {
        this.visitNum++;
        return this.visitNum;
    }

    void cleanNode(long v) {
        getNode(v).clean();
    }
}
