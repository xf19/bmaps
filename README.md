# README #

### What is this repository for? ###

This is a web mapping application; front end code was already provided, and the project involved writing the back end code

Implemented code:

The Rasterer class takes as input an upper left latitude and longitude, a lower right latitude and longitude, a window width, and a window height. 
Using these six numbers, it produces a 2D array of filenames corresponding to the files to be rendered.

The GraphDB class reads in testing dataset and stores it as a graph. 
Each node in the graph represents a single intersection, and each edge represents a road. 

The Router class takes as input a GraphDB, a starting latitude and longitude, and a destination latitude and longitude.
It produces a list of nodes (i.e. intersections) that gets you from the start point to the end point. (A* search algorithm, using an explicit graph object).

